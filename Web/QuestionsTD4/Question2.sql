CREATE TABLE Utilisateur (
  idUtilisateur INT(11) NOT NULL AUTO_INCREMENT,
  email         VARCHAR(50),
  nom           VARCHAR(50),
  prenom        VARCHAR(50),
  adresse       VARCHAR(70),
  num           VARCHAR(50),
  dateNaissance DATE,
  PRIMARY KEY (idUtilisateur)
);

CREATE TABLE Commentaire (
  idCommentaire INT(11) NOT NULL AUTO_INCREMENT,
  titre         VARCHAR(50),
  contenu       VARCHAR(250),
  dateCreation  DATE,
  idUtilisateur INT(11),
  idJeu         INT(11),
  created_at    DATETIME,
  updated_at    DATETIME,
  PRIMARY KEY (idCommentaire),
  FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur (idUtilisateur),
  FOREIGN KEY (idJeu) REFERENCES Game (id)
);
