<?php

require_once '../vendor/autoload.php';

use gamepedia\models\Game;
use gamepedia\models\Utilisateur;
use gamepedia\models\Commentaire;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$faker = Faker\Factory::create('fr_FR');

$jeux = Game::all();
$nbJeux = $jeux->count();

for ($i=1; $i <= 250; $i++){
  $utilisateur = new Utilisateur();
  $utilisateur->prenom = $faker->firstName($gender = null);
  $utilisateur->nom = $faker->lastName;
  $utilisateur->email = $faker->email;
  $utilisateur->adresse = $faker->address;
  $utilisateur->num = $faker->phoneNumber;
  $utilisateur->dateNaissance = $faker->dateTime($max = 'now');
  $utilisateur->save();

  for ($j=1; $j <= 10; $j++){
    $com = new Commentaire();
    $com->titre = $faker->realText($maxNbChars = 30);
    $com->contenu = $faker->realText($maxNbChars = 200);
    $com->dateCreation = $faker->dateTime($max = 'now');

    $jeu = $jeux[rand(1,$nbJeux)];
    $com->surlejeu()->associate($jeu);
    $com->ecritpar()->associate($utilisateur);

    $com->save();
  }
}
