<?php

use gamepedia\models\Utilisateur;
use gamepedia\models\Commentaire;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


try {
  $utilisateur1 = new Utilisateur();
  $utilisateur1->prenom = "Pierrot";
  $utilisateur1->nom = "Tisserie";
  $utilisateur1->num = "1";
  $utilisateur1->email = "pierrot@mail.com";
  $utilisateur1->adresse = "Nancy";
  $utilisateur1->dateNaissance = new DateTime('1991-11-11');
  $utilisateur1->save();


  $com1 = new Commentaire();
  $com1->contenu = "voici mon premier comm";
  //$com1->created_at = now();
  //$com1->dateCreation = now();
  $com1->idJeu = "12342";
  $com1->idUtilisateur = "1";
  $com1->idCommentaire = "1";
  $com1->titre = "Premier Commentaire";
  //$com1->updated_at = now();
  $com1->save();

  $com2 = new Commentaire();
  $com2->contenu = "agittis diam. Pellentesque";
  //$com2->created_at = now();
  //$com2->dateCreation = now();
  $com2->idJeu = "12342";
  $com2->idUtilisateur = "1";
  $com2->idCommentaire = "2";
  $com2->titre = "Le deuxieme";
  //$com2->updated_at = now();
  $com2->save();

  $com3 = new Commentaire();
  $com3->contenu = "nsequat vehicula id sit amet ex.";
  //$com3->created_at = now();
  //$com3->dateCreation = now();
  $com3->idJeu = "12342";
  $com3->idUtilisateur = "1";
  $com3->idCommentaire = "3";
  $com3->titre = "Le dernier";
  //$com3->updated_at = now();
  $com3->save();

  $utlisateur2 = new Utilisateur();
  $utlisateur2->prenom = "Theo";
  $utlisateur2->nom = "Tiste";
  $utlisateur2->num = "2";
  $utlisateur2->email = "Theo@mail.fr";
  $utlisateur2->adresse = "Epinal";
  $utlisateur2->dateNaissance = new DateTime('1999-05-02');
  $utlisateur2->save();

  $com1 = new Commentaire();
  $com1->contenu = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
  //$com1->created_at = now();
  //$com1->dateCreation = now();
  $com1->idJeu = "12342";
  $com1->idUtilisateur = "2";
  $com1->idCommentaire = "4";
  $com1->titre = "Un nouveau Commentaire";
  //$com1->updated_at = now();
  $com1->save();

  $com2 = new Commentaire();
  $com2->contenu = "L'avantage du Lorem Ipsum sur un texte générique comme.";
  //$com2->created_at = now();
  //$com2->dateCreation = now();
  $com2->idJeu = "12342";
  $com2->idUtilisateur = "2";
  $com2->idCommentaire = "5";
  $com2->titre = "Un autre";
  //$com2->updated_at = now();
  $com2->save();

  $com3 = new Commentaire();
  $com3->contenu = "Le Lorem Ipsum est le faux texte standard.";
  //$com3->created_at = now();
  //$com3->dateCreation = now();
  $com3->idJeu = "12342";
  $com3->idUtilisateur = "2";
  $com3->idCommentaire = "6";
  $com3->titre = "et encore un autre";
  //$com3->updated_at = now();
  $com3->save();

  echo "lignes inserées";
} catch (\Exception $e) {
  var_dump($e);
}
