<?php

require_once '../vendor/autoload.php';

use gamepedia\models\Utilisateur;
use gamepedia\models\Commentaire;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$comments = Utilisateur::where('idUtilisateur','=',1)->with('commentaires')->get();

foreach ($comments as $c) {
  foreach ($c->commentaires as $value) {
    echo "Titre : ".$value->titre." - Date : ".$value->dateCreation." \n";
  }
}
