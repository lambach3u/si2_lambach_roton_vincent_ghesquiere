<?php

require_once '../vendor/autoload.php';

use gamepedia\models\Utilisateur;
use gamepedia\models\Commentaire;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$utilisateurs = Utilisateur::whereHas('commentaires', function ($query){
    $query->havingRaw('COUNT(*) > 5');
  })->get();

foreach ($utilisateurs as $u) {
  echo $u->nom." ".$u->prenom." \n";
}
