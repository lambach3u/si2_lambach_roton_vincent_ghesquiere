<?php
use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$time_start = microtime(true);
$jeux = Game::where('name', 'like', 'Mario%')->get();

foreach ($jeux as $jeu) {
    $jrs = $jeu->ratings()->where('name', 'like', '%3+%')->get();
}
$time_end = microtime(true);

echo "Temps pour lister tous les jeux contenant mario et ayant un rating initial de 3+ : ".($time_end - $time_start)." secondes";
