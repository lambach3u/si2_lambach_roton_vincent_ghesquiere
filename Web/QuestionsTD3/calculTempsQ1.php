<?php
use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$time_start = microtime(true);
Game::all();
$time_end = microtime(true);

echo "Temps pour lister toutes les questions : ".($time_end - $time_start)." secondes";
