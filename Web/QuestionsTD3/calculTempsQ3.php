<?php
use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$time_start = microtime(true);
$games = Game::where('name', 'like', 'Mario%')->get();
foreach ($games as $g) {
    $persos = $g->characters();
    if ($persos == null)
        continue;

    $persos = $persos->get();
}
$time_end = microtime(true);

echo "Temps pour lister tous les jeux contenant mario : ".($time_end - $time_start)." secondes";
