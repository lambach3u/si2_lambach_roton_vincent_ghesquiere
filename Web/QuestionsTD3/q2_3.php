<?php

use gamepedia\models\Company;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();



$time_start = microtime(true);
Company::where('location_country', 'like', 'France')->get();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Companies from France : ".$time."\n";

//sans index : 0.047852993011475  0.053524971008301  0.056326150894165
//avec index : 0.039016008377075  0.058767080307007  0.040206909179688

//la presence d'un index n'a pas d'impact notable

 ?>
