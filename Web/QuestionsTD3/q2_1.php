<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();



$time_start = microtime(true);
Game::where('name', 'like', 'Mario%')->get();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Jeux qui commence par Mario : ".$time."\n";

$time_start = microtime(true);
Game::where('name', 'like', 'The Elder Scrolls%')->get();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Jeux qui commence par The Elder Scrolls : ".$time."\n";

$time_start = microtime(true);
Game::where('name', 'like', 'Sonic%')->get();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Jeux qui commence par Sonic : ".$time."\n";

//lindex permet de diviser le temps dexecution par 100
//cependant il n'influe pas sur les requetes 'contient' car le trie par ordre alphabetique n'a aucun interet dans ce cas

 ?>
