<?php
use gamepedia\models\Game;
use gamepedia\models\Company;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$db->getConnection()->enableQueryLog();

$games = Company::where('name', 'like', '%Sony%')->with('developedGames')->get();

$queries = $db->getConnection()->getQueryLog();
displayLog($queries);

function displayLog($queries){
    $i = 1;
    foreach ($queries as $q){
        echo " + Requête ".$i." : \n   ".$q["query"]."\n\n";
        echo "   Paramètres : \n";
        $bindings = $q["bindings"];
        foreach ($bindings as $b){
            echo "   - ".$b."\n";
        }
        echo "\n Temps d'exécution : ".$q["time"]."\n\n";
        $i++;
    }
}