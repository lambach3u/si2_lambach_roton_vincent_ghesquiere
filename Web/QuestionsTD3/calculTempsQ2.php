<?php
use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();



$time_start = microtime(true);
Game::where('name','like','%Mario%')->get();
$time_end = microtime(true);

echo "Temps pour lister tous les jeux contenant mario : ".($time_end - $time_start)." secondes";
