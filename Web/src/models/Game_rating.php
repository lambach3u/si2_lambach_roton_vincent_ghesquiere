<?php

namespace gamepedia\models;

class Game_rating extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'game_rating';
    protected $primaryKey = 'id';

    public function board(){
      return $this->belongsTo(Rating_board::class,"rating_board_id");
    }

    public function game() {
        return $this->belongsToMany(Game::class, "game2rating", "rating_id", "game_id");
    }
}
