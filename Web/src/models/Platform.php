<?php

namespace gamepedia\models;

class Platform extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'platform';
    protected $primaryKey = 'id';

    public function games() {
        return $this->belongsToMany(Game::class, "game2platform", "platform_id", "game_id");
    }
}
