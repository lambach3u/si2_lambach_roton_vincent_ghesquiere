<?php

namespace gamepedia\models;

class Rating_board extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'rating_board';
    protected $primaryKey = 'id';

    public function notes() {
      return $this->hasMany(Game_rating::class,"rating_board_id");
    }

}
