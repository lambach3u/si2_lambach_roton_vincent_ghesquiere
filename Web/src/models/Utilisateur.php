<?php

namespace gamepedia\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'utilisateur';
    protected $primaryKey = 'idUtilisateur';
    public $timestamps = false;

    public function commentaires() {
        return $this->hasMany(Commentaire::class,"idUtilisateur");
    }
}
