<?php

namespace gamepedia\models;

class Character extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'character';
    protected $primaryKey = 'id';

    public function games() {
        return $this->belongsToMany(Game::class, "game2character", "character_id", "game_id");
    }

    public function friends() {
        return $this->belongsToMany(Character::class, "friends", "char1_id", "char2_id");
    }

    public function enemies() {
        return $this->belongsToMany(Character::class, "enemies", "char1_id", "char2_id");
    }

    public function premiereApparence() {
        return $this->belongsTo(Character::class,"first_appeared_in_game_id");
    }
}
