<?php

namespace gamepedia\models;

class Game extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function characters() {
        return $this->belongsToMany(Character::class, "game2character", "character_id" , "game_id");
    }

    public function genres() {
        return $this->belongsToMany(Genre::class, "game2genre", "game_id", "genre_id");
    }

    public function platforms() {
        return $this->belongsToMany(Platform::class, "game2platform", "game_id", "platform_id");
    }

    public function ratings() {
        return $this->belongsToMany(Game_rating::class, "game2rating", "game_id", "rating_id");
    }

    public function themes() {
        return $this->belongsToMany(Theme::class, "game2theme", "game_id", "theme_id");
    }

    public function developers() {
        return $this->belongsToMany(Company::class, "game_developers", "game_id", "comp_id");
    }

    public function publishers() {
        return $this->belongsToMany(Company::class, "game_publishers", "game_id", "comp_id");
    }

    public function similarGames() {
        return $this->belongsToMany(Game::class, "similar_games", "game1_id", "game2_id");
    }

    public function premieresApparitions() {
        return $this->hasMany(Character::class,"first_appeared_in_game_id");
    }

    public function commentaires() {
        return $this->hasMany(Commentaire::class,"idJeu");
    }
}
