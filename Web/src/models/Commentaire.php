<?php

namespace gamepedia\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'commentaire';
    protected $primaryKey = 'idCommentaire';
    public $timestamps = true;

    public function ecritpar() {
        return $this->belongsTo(Utilisateur::class,"idUtilisateur");
    }

    public function surlejeu() {
        return $this->belongsTo(Game::class,"idJeu");
    }
}
