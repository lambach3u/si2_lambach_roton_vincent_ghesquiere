<?php

use gamepedia\models\Company;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$comp =  Company::where('location_country', '=', 'Japan')->get();
foreach ($comp as $company) {
  echo $company["name"]."\n";
}
