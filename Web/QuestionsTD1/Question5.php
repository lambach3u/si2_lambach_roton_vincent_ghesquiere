<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$pages = Game::select("id")->orderBy('id','desc')->first();

$jeux = Game::select("id","name","deck")->where('id','>',($argv[1]-1)*500)->where('id','<=',$argv[1]*500)->get();
foreach ($jeux as $jeu) {
  echo $jeu['id']." - ".$jeu['name']." - ".$jeu['deck']."\n";
}

echo "\n   Page ".$argv[1]." sur ".round($pages['id']/500,0,PHP_ROUND_HALF_UP);
