<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$jeux =  Game::skip(21173)->take(442)->get();
foreach ($jeux as $jeu) {
  echo $jeu["name"]."\n";
}
