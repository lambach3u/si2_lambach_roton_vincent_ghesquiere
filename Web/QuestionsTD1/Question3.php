<?php

use gamepedia\models\Platform;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$platformes = Platform::where('install_base','>','10000000')->get();
foreach ($platformes as $platforme) {
   echo $platforme["abbreviation"]." -  Base : ".$platforme["install_base"]."\n";
}
