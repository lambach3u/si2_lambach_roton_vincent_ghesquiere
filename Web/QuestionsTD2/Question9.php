<?php

use gamepedia\models\Game;
use gamepedia\models\Genre;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$genre = new Genre();

$genre->name = "CMS";
$genre->description = "Construction and management simulation (CMS) is a type of simulation game in which players build, expand or manage fictional communities or projects with limited resources.";

Game::where("id", "=", 12)->first()->genres()->save($genre);
Game::where("id", "=", 56)->first()->genres()->save($genre);
Game::where("id", "=", 345)->first()->genres()->save($genre);
