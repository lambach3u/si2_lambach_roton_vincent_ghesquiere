<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$games = Game::where('name', 'like', '%Mario%')->get();
foreach ($games as $g) {
    $rating = $g->ratings()->first();
    if ($rating == null)
        continue;

    $board = $rating->board()->first();
    echo $g['name'] . " - Rating : " . $rating['name'] . " - Board : " . $board['name'] . "\n";
}
