<?php

use gamepedia\models\Company;
use gamepedia\models\Game;

require '../vendor/autoload.php';
$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$jeux = Game::where('name', 'like', 'Mario%')->get();
foreach ($jeux as $jeu) {
  $jrs = $jeu->ratings()->where('name', 'like', '%3+%')->get();
  if (sizeof($jrs) > 0 && $jrs[0] != null){
    $publishers = $jeu->publishers()->where('name','like','%Inc%')->get();
    if (sizeof($publishers) > 0 && $publishers[0] != null) {
      echo $jeu["name"]."\n";
    }
  }
}
