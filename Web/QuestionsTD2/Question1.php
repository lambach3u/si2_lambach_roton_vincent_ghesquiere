<?php

use gamepedia\models\Game;
use gamepedia\models\Character;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$jeux =  Game::skip(12341)->take(1)->get();
foreach ($jeux as $jeu) {
  $persos = $jeu->characters()->get();
  foreach ($persos as $perso) {
    echo $perso["name"]."  ".$perso["deck"]."\n";
  }
}
