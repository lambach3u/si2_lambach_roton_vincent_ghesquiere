<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$jeux = Game::where('name', 'like', 'Mario%')->get();
foreach ($jeux as $jeu) {
  $ratings = $jeu->ratings()->where('name', 'like', '%3+%')->get();
  foreach ($ratings as $value) {
    $board = $value->board()->where('name','like','%CERO%')->get();
    if ($board != null)
        continue;
  }
  if (sizeof($ratings) > 0 && $ratings[0] != null && $board != null){
    $publishers = $jeu->publishers()->where('name','like','%Inc%')->get();
    if (sizeof($publishers) > 0 && $publishers[0] != null) {
      echo $jeu["name"]."\n";
    }
  }
}
