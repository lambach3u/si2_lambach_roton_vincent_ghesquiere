<?php

use gamepedia\models\Company;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$companies = Company::where('name', 'like', '%Sony%')->get();

foreach ($companies as $company) {
    $jeux = $company->developedGames()->get();
    foreach ($jeux as $jeu) {
        echo $jeu["name"] . "\n";
    }
}
