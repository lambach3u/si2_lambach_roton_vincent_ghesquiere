<?php

use gamepedia\models\Game;

require '../vendor/autoload.php';

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('../src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$jeux = Game::where('name', 'like', 'Mario%')->get();

foreach ($jeux as $jeu) {
    $count = $jeu->characters()->count();
    if ($count > 3)
        echo $jeu["name"] . "\n";
}
